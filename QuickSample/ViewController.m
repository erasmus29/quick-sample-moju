//
//  ViewController.m
//  QuickSample
//
//  Created by Erasmus Trinidad on 3/16/15.
//  Copyright (c) 2015 Erasmus Trinidad. All rights reserved.
//

#import "ViewController.h"
#import "AnimateImageWithGyro.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    AnimateImageWithGyro *tempImage = [[AnimateImageWithGyro alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andData:@{@"filename":@"keboard_", @"count":@"26", @"filetype":@"jpg"}];
    [tempImage setCenter:self.view.center];
    [tempImage setBackgroundColor:[UIColor lightGrayColor]];
    [self.view addSubview:tempImage];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

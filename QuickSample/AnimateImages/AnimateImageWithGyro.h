//
//  AnimateImageWithGyroVC.h
//  QuickSample
//
//  Created by Erasmus Trinidad on 3/16/15.
//  Copyright (c) 2015 Erasmus Trinidad. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreMotion;

@interface AnimateImageWithGyro : UIImageView{
    
}
- (id)initWithFrame:(CGRect)frame andData: (NSDictionary *)data;

@property (nonatomic) CGRect tempFrame;
@property (nonatomic, retain) NSDictionary *tempData;
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) NSOperationQueue *deviceQueue;
@property (nonatomic) CGFloat tempCurrentValue;
@property (nonatomic) NSInteger currentImageIndex;
@property (nonatomic, retain) NSMutableArray *tempArray;

@end

//
//  AnimateImageWithGyroVC.m
//  QuickSample
//
//  Created by Erasmus Trinidad on 3/16/15.
//  Copyright (c) 2015 Erasmus Trinidad. All rights reserved.
//

#import "AnimateImageWithGyro.h"

#define kValueGyroSensitivity       0.51 // 2.01 - slower     0.01 - faster

@interface AnimateImageWithGyro ()

@end

@implementation AnimateImageWithGyro
@synthesize tempFrame, tempData, motionManager, deviceQueue, tempCurrentValue, currentImageIndex, tempArray;

- (id)initWithFrame:(CGRect)frame andData: (NSDictionary *)data{
    if (self = [super initWithFrame:frame]) {
        tempFrame = frame;
        tempData = data;
        currentImageIndex = 0;
        [self setup];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}



- (void)setup {
    [self setContentMode:UIViewContentModeScaleAspectFit];
    
    self.tempArray = [[NSMutableArray alloc] init];
    int tempCount = [tempData[@"count"] intValue];
    
    for (int i = 0; i < tempCount; i++){
        NSString *fileName = [NSString stringWithFormat:@"%@%05i.%@",tempData[@"filename"], i+1, tempData[@"filetype"]];
//        NSData *imgData = UIImageJPEGRepresentation([self scaleToSize:1080 withImage:[UIImage imageNamed:fileName]],1);
        
//        if (!imgData) continue;
//        float maxValue = 100000;
//        float compressionRatio = 1 / ([imgData length]/maxValue);
//        
//        NSData *newData;
//        if ([imgData length] > maxValue) {
//            newData = UIImageJPEGRepresentation([UIImage imageNamed:fileName],compressionRatio);
//        }
//        else {
//            newData = imgData;
//        }
//        NSString *filePath;
//        NSString *imageCachePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]stringByAppendingPathComponent:@"ImageCache"];
//        filePath = [imageCachePath stringByAppendingPathComponent:fileName];
//        [newData writeToFile:filePath atomically:TRUE];
//        
//        newData = nil;
        
//        if([fileName isEqualToString:@""]) continue;
        [self.tempArray addObject:fileName];
    }
    
    self.deviceQueue = [[NSOperationQueue alloc] init];
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.deviceMotionUpdateInterval = 50/60;
    
    [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryZVertical
                                                            toQueue:self.deviceQueue
                                                        withHandler:^(CMDeviceMotion *motion, NSError *error)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            CGFloat x = motion.gravity.x;
            CGFloat y = motion.gravity.y;
            CGFloat z = motion.gravity.z;
            
            CGFloat r = sqrtf(x*x + y*y + z*z);
            CGFloat tiltLeftRight = acosf(y/r) * 180.0f / M_PI - 90.0f;
            
            NSLog(@"tiltForwardBackward: %f", tiltLeftRight);
            [self tiltControl:tiltLeftRight];
            
        }];
    }];
    
    // show on mid
    currentImageIndex = [tempArray count] / 2;
    [self showImageAtIndex:currentImageIndex];
    
}

- (void) tiltControl : (CGFloat) tiltLeftRight{
    
//    NSLog(@"tiltLeftRight: %f", tiltLeftRight);
//    return;
    
    float roundingValue = kValueGyroSensitivity, roundOffValue;
    int mulitpler = floor(tiltLeftRight / roundingValue);
    roundOffValue = mulitpler * roundingValue;
    
    NSLog(@"tiltForwardBackward: %f", roundOffValue);
    if (roundOffValue > tempCurrentValue) {
//        NSLog(@"previous!");
        currentImageIndex--;
        if (currentImageIndex < 0){
            currentImageIndex = 0;
            tempCurrentValue = roundOffValue-kValueGyroSensitivity;
        }
        else{
            tempCurrentValue = roundOffValue;
        }
        
        [self showImageAtIndex:currentImageIndex];
    }
    else if (roundOffValue < tempCurrentValue) {
//        NSLog(@"next!");
        currentImageIndex++;
        if (currentImageIndex > [self.tempArray count]-1){
            currentImageIndex = [self.tempArray count]-1;
            tempCurrentValue = roundOffValue+kValueGyroSensitivity;
        }
        else{
            tempCurrentValue = roundOffValue;
        }
        
        [self showImageAtIndex:currentImageIndex];
    }

    
}

- (void) showImageAtIndex : (NSInteger) indexVal{

    if (indexVal > [self.tempArray count]) return;
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.3;
    [self.layer addAnimation:transition forKey:nil];
//    NSLog(@"fileName: %@", self.tempArray[indexVal]);
    self.image = [UIImage imageNamed:self.tempArray[indexVal]];

    
    [self setNeedsLayout];
    
    
    
}

- (UIImage *) scaleToSize:(CGFloat) size withImage:(UIImage*)image {
    
    CGFloat width = size;
    CGFloat height = size;
    //ScaleImage here
    if (image.size.width > image.size.height) {
        //900 width
        
        height = ((image.size.height/image.size.width)*size);
    }
    else
    {
        //900 height
        width = ((image.size.width/image.size.height)*size);
    }
    
    //    NSLog(@"w %f h %f", width, height);
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    
    
    UIGraphicsBeginImageContext(bounds.size);
    [image drawInRect:CGRectMake(0.0, 0.0, bounds.size.width, bounds.size.height)];
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

@end

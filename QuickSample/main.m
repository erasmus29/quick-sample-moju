//
//  main.m
//  QuickSample
//
//  Created by Erasmus Trinidad on 3/16/15.
//  Copyright (c) 2015 Erasmus Trinidad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
